;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Open Socket
alias -l opensocket {
  if ($regex($2-, /((?:https?\://)?(?:\w+\.)?(?>youtube|youtu)(?:\.\w+){1,2}/)/i)) {
    var urldomain $regml(1)
    if ($regex($2-, /(?:=|%3F)?v(?:=|%3D)([\w\-]+)/i)) var %urlmatch = $regml(1)
    else if ($regex($2-, /(?:(?:https?\://)?(?:\w+\.)?(?>youtube|youtu)(?:\.\w+){1,2}/)([\w\-]+)/i)) {
      var %urlmatch = $regml(1)
    }
    else return
    var %urlticks = $ticks
    if ($sock($+(url., %urlticks))) sockclose $+(url., %urlticks)
    set %urltarget $1
    sockopen $+(url., %urlticks) gdata.youtube.com 80
    sockmark $+(url., %urlticks) %urlmatch
  }
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Channel Triggers
on *:TEXT:*:#: {
  opensocket $chan $1-
}

on *:ACTION:*:#: {
  opensocket $chan $1-
}

on *:INPUT:#: {
  opensocket $chan $1-
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Query Triggers
on *:TEXT:*:?: {
  ;  opensocket $nick $1-
}

on *:ACTION:*:?: {
  opensocket $nick $1-
}

on *:INPUT:?: {
  opensocket $nick $1-
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Socket Triggers
on *:SOCKOPEN:url.*: {
  if ($sockerr > 0) halt
  sockwrite -nt $sockname GET $+(/feeds/api/videos?&v=2.1&safeSearch=none&restriction=255.255.255.255&q=", $sock($sockname).mark, "&fields=entry[media:group/yt:videoid=", $sock($sockname).mark, "](title)) HTTP/1.1
  sockwrite -nt $sockname Host: gdata.youtube.com
  sockwrite -nt $sockname Connection: close
  sockwrite -nt $sockname Content-Type: text/xml
  sockwrite -nt $sockname $crlf
}

on *:SOCKREAD:url.*: {
  if ($sockerr > 0) return
  var %urlreader
  sockread %urlreader
  if ($regex(%urlreader, /<title>(.*?)<\/title>/si)) {
    echo 10 -gnt %urltarget <***> Youtube: $regml(1)
    sockclose $sockname
    unset %url*
  }
}

; This is only triggered when the remote client/server closes the connection
on *:SOCKCLOSE:url.*: {
  unset %url*
}
