on $^*:HOTLINK:/#(\d+)/:#mpc-hc, #mpc-hc-dev: return
on $*:HOTLINK:/#(\d+)/:#mpc-hc, #mpc-hc-dev: {
  url http://sourceforge.net/apps/trac/mpc-hc/ticket/ $+ $regml(1)
}

on $^*:HOTLINK:/\b(?>rev\.|r)(\d+)\b/i:#mpc-hc, #mpc-hc-dev: return
on $*:HOTLINK:/\b(?>rev\.|r)(\d+)\b/i:#mpc-hc, #mpc-hc-dev: {
  url http://sourceforge.net/apps/trac/mpc-hc/changeset/ $+ $regml(1)
}
