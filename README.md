Description
------------

 - *get-youtube-video-title.mrc* -- Shows video titles for Youtube urls pasted on channels.  
   Great for avoiding rickrolls and NSFL videos. Script working with *youtu.be* and *youtube.com* urls.
 - *mpc-hc-hotlinks.mrc* -- Makes ticket and revision numbers from the CIA bot in the mpc-hc channels clickable.